package com.ominext.hdviet.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ominext.hdviet.R;
import com.ominext.hdviet.UI.activity.DetailActivity;
import com.ominext.hdviet.model.category.movieofcategory.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by truong vu on 10/3/2016.
 */
public class MovieOfCategoryAdapter extends RecyclerView.Adapter<MovieOfCategoryAdapter.ViewHolder> {

    public static final String MOVIE_ID = "MOVIE_ID";

    private List<com.ominext.hdviet.model.category.homepage.Movie> listPhimCapNhat;
    private LayoutInflater inflater;
    private List<Movie> itemList;
    private Context mContext;
    private int sizeList;
    private int itemCount;
    private int idCategory = 1;

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public MovieOfCategoryAdapter(Context context, List<Movie> itemList) {
        mContext = context;
        this.itemList = itemList;
        itemCount = itemList.size();
        sizeList = itemList.size();
        inflater = LayoutInflater.from(context);
    }

    public MovieOfCategoryAdapter(Context context, List<com.ominext.hdviet.model.category.homepage.Movie> listPhimCapNhat, int idPhimCapNhat) {
        mContext = context;
        idCategory = idPhimCapNhat;
        sizeList = listPhimCapNhat.size();
        itemCount = listPhimCapNhat.size();
        this.listPhimCapNhat = listPhimCapNhat;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = inflater.inflate(R.layout.layout_item_movie, parent, false);
        ViewHolder holder = new ViewHolder(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, DetailActivity.class);

                if(idCategory>0){
                    intent.putExtra(MOVIE_ID, itemList.get(position).getMovieID());
                }else{
                    intent.putExtra(MOVIE_ID, listPhimCapNhat.get(position).getMovieID());
                }
                mContext.startActivity(intent);
            }
        });

        if (idCategory>0){
            Movie item = itemList.get(position);
            holder.movieName.setText(item.getMovieName());
            Picasso.with(mContext).load(item.getPoster214x321()).into(holder.moviePhoto);
        }else if (idCategory==-1){
            com.ominext.hdviet.model.category.homepage.Movie item = listPhimCapNhat.get(position);
            holder.movieName.setText(item.getMovieName());
            Picasso.with(mContext).load(item.getPoster214x321()).into(holder.moviePhoto);
        }
    }


    @Override
    public int getItemCount() {
        return itemCount;
    }

    public int getSizeList() {
            return sizeList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView movieName;
        ImageView moviePhoto;

        public ViewHolder(View itemView) {
            super(itemView);
            movieName = (TextView) itemView.findViewById(R.id.movie_name);
            moviePhoto = (ImageView) itemView.findViewById(R.id.movie_photo);
        }
    }


}
