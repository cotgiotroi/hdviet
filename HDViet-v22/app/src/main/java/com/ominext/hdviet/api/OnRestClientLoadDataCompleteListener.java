package com.ominext.hdviet.api;

import com.ominext.hdviet.model.category.category.CategoryAPI;
import com.ominext.hdviet.model.category.homepage.HomepageAPI;
import com.ominext.hdviet.model.category.moviedetail.MovieDetailAPI;
import com.ominext.hdviet.model.category.movieofcategory.MovieOfCategoryAPI;
import com.ominext.hdviet.model.category.movieplay.MoviePlayAPI;
import com.ominext.hdviet.model.category.search.SearchAPI;

/**
 * Created by 20133 on 10/6/2016.
 */

public interface OnRestClientLoadDataCompleteListener {
    void onLoadCategoryComplete(CategoryAPI categoryAPI);
    void onLoadHomepageComplete(HomepageAPI homepageAPI);
    void onLoadMovieDetailComplete(MovieDetailAPI movieDetailAPI);
    void onLoadMovieOfCategoryComplete(MovieOfCategoryAPI movieOfCategoryAPI);
    void onLoadMoviePlayComplete(MoviePlayAPI moviePlayAPI);
    void onLoadDataSearchComplete(SearchAPI searchAPI);
}
