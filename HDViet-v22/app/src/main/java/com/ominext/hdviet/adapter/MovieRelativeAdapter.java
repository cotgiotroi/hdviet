package com.ominext.hdviet.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ominext.hdviet.R;
import com.ominext.hdviet.UI.activity.DetailActivity;
import com.ominext.hdviet.model.category.moviedetail.Relative;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.ominext.hdviet.adapter.MovieOfCategoryAdapter.MOVIE_ID;

/**
 * Created by 20133 on 10/19/2016.
 */

public class MovieRelativeAdapter extends RecyclerView.Adapter<MovieRelativeAdapter.ViewHolder> {

    private List<Relative> arrRelatives;
    private Context mContext;

    public MovieRelativeAdapter(List<Relative> arrRelatives, Context mContext) {
        this.arrRelatives = arrRelatives;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(parent, LayoutInflater.from(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.itemView.setClickable(true);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra(MOVIE_ID, arrRelatives.get(position).getMovieID());
                mContext.startActivity(intent);
            }
        });
        Picasso.with(mContext).load(arrRelatives.get(position).getPoster()).into(holder.imageView);
        holder.textView.setText(arrRelatives.get(position).getKnownAs());
    }

    @Override
    public int getItemCount() {
        return arrRelatives.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;

        public ViewHolder(ViewGroup viewGroup, LayoutInflater inflater) {
            super(inflater.inflate(R.layout.layout_item_movie, viewGroup, false));
            imageView = (ImageView) itemView.findViewById(R.id.movie_photo);
            textView = (TextView) itemView.findViewById(R.id.movie_name);
        }
    }
}
