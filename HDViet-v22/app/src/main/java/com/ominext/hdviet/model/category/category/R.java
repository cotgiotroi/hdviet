
package com.ominext.hdviet.model.category.category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class R {

    @SerializedName("CategoryID")
    @Expose
    private String categoryID;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;
    @SerializedName("Subcate")
    @Expose
    private Integer subcate;

    /**
     * No args constructor for use in serialization
     * 
     */
    public R() {
    }

    /**
     * 
     * @param categoryName
     * @param categoryID
     * @param subcate
     */
    public R(String categoryID, String categoryName, Integer subcate) {
        this.categoryID = categoryID;
        this.categoryName = categoryName;
        this.subcate = subcate;
    }

    /**
     * 
     * @return
     *     The categoryID
     */
    public String getCategoryID() {
        return categoryID;
    }

    /**
     * 
     * @param categoryID
     *     The CategoryID
     */
    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    /**
     * 
     * @return
     *     The categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * 
     * @param categoryName
     *     The CategoryName
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * 
     * @return
     *     The subcate
     */
    public Integer getSubcate() {
        return subcate;
    }

    /**
     * 
     * @param subcate
     *     The Subcate
     */
    public void setSubcate(Integer subcate) {
        this.subcate = subcate;
    }

    @Override
    public String toString() {
        return "ID: "+categoryID + ", name: "+categoryName + "\n";
    }

}
