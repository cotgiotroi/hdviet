package com.ominext.hdviet.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ominext.hdviet.R;
import com.ominext.hdviet.UI.activity.DetailActivity;
import com.ominext.hdviet.model.category.MovieSaved;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static android.content.Context.MODE_APPEND;
import static com.ominext.hdviet.UI.activity.DetailActivity.MOVIE_SAVED;
import static com.ominext.hdviet.adapter.MovieOfCategoryAdapter.MOVIE_ID;

/**
 * Created by 20133 on 10/21/2016.
 */

public class MovieSavedAdapter extends RecyclerView.Adapter<MovieSavedAdapter.ViewHolder>{


    private List<MovieSaved> arrMovie;
    private Context mContext;

    public MovieSavedAdapter(Context mContext) {
        this.mContext = mContext;
        getData();

    }

    private void getData() {
        arrMovie = new ArrayList<>();
        SharedPreferences preferences = mContext.getSharedPreferences(MOVIE_SAVED,MODE_APPEND);
        Set<String> stringSet = preferences.getStringSet(MOVIE_SAVED, null);
        Iterator<String> iterator = stringSet.iterator();

        while (iterator.hasNext()){
            String s = iterator.next();
            String[] strings = s.split("~");
            String id = strings[0];
            String name = strings[1];
            String runtime = strings[2];
            String imdrating = strings[3];
            String linkBG= strings[4];
            arrMovie.add(new MovieSaved(id,name,runtime,imdrating,linkBG));
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(parent,LayoutInflater.from(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.tvDuration.setText("Thời gian: "+arrMovie.get(position).getDuration() + " phút");
        holder.tvRate.setText(arrMovie.get(position).getRate());
        holder.tvName.setText(arrMovie.get(position).getName());
        Picasso.with(mContext).load(arrMovie.get(position).getLinkBG()).into(holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra(MOVIE_ID, arrMovie.get(position).getId());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrMovie.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView tvName,tvDuration,tvRate;
        ImageView imageView;

        public ViewHolder(ViewGroup group, LayoutInflater inflater) {
            super(inflater.inflate(R.layout.layout_item_movie_saved,group,false));

            tvRate = (TextView) itemView.findViewById(R.id.tv_imd_rating);
            tvDuration= (TextView) itemView.findViewById(R.id.tv_duration);
            tvName= (TextView) itemView.findViewById(R.id.tv_movie_name_saved);
            imageView = (ImageView) itemView.findViewById(R.id.img);

        }
    }
}
