
package com.ominext.hdviet.model.category.search;

import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("MovieID")
    private String movieID;

    @SerializedName("MovieName")
    private String movieName;

    @SerializedName("KnownAs")
    private String knownAs;

    public Data(String movieID, String movieName, String knownAs) {
        this.movieID = movieID;
        this.movieName = movieName;
        this.knownAs = knownAs;
    }

    public String getMovieID() {
        return movieID;
    }

    public void setMovieID(String movieID) {
        this.movieID = movieID;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getKnownAs() {
        return knownAs;
    }

    public void setKnownAs(String knownAs) {
        this.knownAs = knownAs;
    }

    @Override
    public String toString() {
        return "Data{" +
                "movieID='" + movieID + '\'' +
                ", movieName='" + movieName + '\'' +
                ", knownAs='" + knownAs + '\'' +
                '}';
    }
}
