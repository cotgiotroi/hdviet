package com.ominext.hdviet.api;

/**
 * Created by 20133 on 10/6/2016.
 */

public final class Config {
    public static final String API_BASE_URL = "https://api-v2.hdviet.com/";
    public static final String SIGN_KEY = "790657bff0963e6e0bf882a763633c7e";
    public static final String PATH_GET_DATA_CATEGORY = "/category/menu";
    public static final String PATH_GET_DATA_HOMEPAGE = "/movie";
    public static final String PATH_GET_DATA_MOVIE_DETAIL = "/movie";
    public static final String PATH_GET_DATA_MOVIE_OF_CATEGORY= "/movie";
    public static final String PATH_GET_DATA_SEARCH = "/movie/search";
    public static final String PATH_GET_DATA_MOVIE_PLAY = "/movie/play";
    public static final String KEY_YOUTUBE_API = "AIzaSyAptfjgMu2d7DE-tOPswRrnBCzbSc_MQS4";
}
