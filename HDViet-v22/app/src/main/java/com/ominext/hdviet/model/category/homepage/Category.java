
package com.ominext.hdviet.model.category.homepage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("CategoryID")
    @Expose
    private String categoryID;
    @SerializedName("CategoryMap")
    @Expose
    private String categoryMap;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;
    @SerializedName("CategoryOrder")
    @Expose
    private String categoryOrder;
    @SerializedName("CategoryStatus")
    @Expose
    private String categoryStatus;
    @SerializedName("WrapLink")
    @Expose
    private String wrapLink;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Category() {
    }

    /**
     * 
     * @param categoryName
     * @param categoryOrder
     * @param wrapLink
     * @param categoryID
     * @param categoryMap
     * @param categoryStatus
     */
    public Category(String categoryID, String categoryMap, String categoryName, String categoryOrder, String categoryStatus, String wrapLink) {
        this.categoryID = categoryID;
        this.categoryMap = categoryMap;
        this.categoryName = categoryName;
        this.categoryOrder = categoryOrder;
        this.categoryStatus = categoryStatus;
        this.wrapLink = wrapLink;
    }

    /**
     * 
     * @return
     *     The categoryID
     */
    public String getCategoryID() {
        return categoryID;
    }

    /**
     * 
     * @param categoryID
     *     The CategoryID
     */
    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    /**
     * 
     * @return
     *     The categoryMap
     */
    public String getCategoryMap() {
        return categoryMap;
    }

    /**
     * 
     * @param categoryMap
     *     The CategoryMap
     */
    public void setCategoryMap(String categoryMap) {
        this.categoryMap = categoryMap;
    }

    /**
     * 
     * @return
     *     The categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * 
     * @param categoryName
     *     The CategoryName
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * 
     * @return
     *     The categoryOrder
     */
    public String getCategoryOrder() {
        return categoryOrder;
    }

    /**
     * 
     * @param categoryOrder
     *     The CategoryOrder
     */
    public void setCategoryOrder(String categoryOrder) {
        this.categoryOrder = categoryOrder;
    }

    /**
     * 
     * @return
     *     The categoryStatus
     */
    public String getCategoryStatus() {
        return categoryStatus;
    }

    /**
     * 
     * @param categoryStatus
     *     The CategoryStatus
     */
    public void setCategoryStatus(String categoryStatus) {
        this.categoryStatus = categoryStatus;
    }

    /**
     * 
     * @return
     *     The wrapLink
     */
    public String getWrapLink() {
        return wrapLink;
    }

    /**
     * 
     * @param wrapLink
     *     The WrapLink
     */
    public void setWrapLink(String wrapLink) {
        this.wrapLink = wrapLink;
    }

}
