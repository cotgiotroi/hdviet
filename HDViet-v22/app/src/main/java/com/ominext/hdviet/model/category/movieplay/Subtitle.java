
package com.ominext.hdviet.model.category.movieplay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subtitle {

    @SerializedName("VIE")
    @Expose
    private VIE__ vIE;
    @SerializedName("ENG")
    @Expose
    private ENG__ eNG;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Subtitle() {
    }

    /**
     * 
     * @param eNG
     * @param vIE
     */
    public Subtitle(VIE__ vIE, ENG__ eNG) {
        this.vIE = vIE;
        this.eNG = eNG;
    }

    /**
     * 
     * @return
     *     The vIE
     */
    public VIE__ getVIE() {
        return vIE;
    }

    /**
     * 
     * @param vIE
     *     The VIE
     */
    public void setVIE(VIE__ vIE) {
        this.vIE = vIE;
    }

    /**
     * 
     * @return
     *     The eNG
     */
    public ENG__ getENG() {
        return eNG;
    }

    /**
     * 
     * @param eNG
     *     The ENG
     */
    public void setENG(ENG__ eNG) {
        this.eNG = eNG;
    }

}
