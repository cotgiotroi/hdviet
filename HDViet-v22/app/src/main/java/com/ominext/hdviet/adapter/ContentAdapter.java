package com.ominext.hdviet.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.ominext.hdviet.R;
import com.ominext.hdviet.model.category.movieofcategory.Movie;
import com.ominext.hdviet.model.category.movieofcategory.MovieOfCategoryAPI;

import java.util.List;

import static com.ominext.hdviet.adapter.MovieOfCategoryAdapter.MOVIE_ID;

/**
 * Created by 20133 on 10/11/2016.
 */

public class ContentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static int TYPE_SLIDER = 1;

    private Context mContext;
    private List<MovieOfCategoryAPI> listMovieOfCategoryAPI;

    public ContentAdapter(List<MovieOfCategoryAPI> listMovieOfCategoryAPI, Context mContext) {
        this.listMovieOfCategoryAPI = listMovieOfCategoryAPI;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_SLIDER) {
            return new SliderViewHolder(parent, LayoutInflater.from(parent.getContext()));
        } else {
            return new CategoryViewHolder(parent, LayoutInflater.from(parent.getContext()));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (position == 0) {

            MovieOfCategoryAPI movieOfCategoryAPI = new MovieOfCategoryAPI(0,listMovieOfCategoryAPI.get(0).getR());
            int sizeMovies = movieOfCategoryAPI.getR().getMovies().size();
            for (int i = 0; i < 6 || i == sizeMovies; i++) {
                Movie movie = movieOfCategoryAPI.getR().getMovies().get(i);
                String linkThumbnail = movie.getNewBackdrop945x530();
                String name = movie.getMovieName();

                TextSliderView textSliderView = new TextSliderView(mContext);
                textSliderView
                        .description(name)
                        .image(linkThumbnail)
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .setOnSliderClickListener((BaseSliderView.OnSliderClickListener) mContext);
                textSliderView
                        .bundle(new Bundle());
                textSliderView
                        .getBundle().putString(MOVIE_ID, movie.getMovieID() + "");

                ((SliderViewHolder)holder).slider.addSlider(textSliderView);
            }

            ((SliderViewHolder)holder).slider.setPresetTransformer(SliderLayout.Transformer.Accordion);
            ((SliderViewHolder)holder).slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            ((SliderViewHolder)holder).slider.setCustomAnimation(new DescriptionAnimation());
            ((SliderViewHolder)holder).slider.setDuration(4000);
            ((SliderViewHolder)holder).slider.startAutoCycle();


        }else{
            ((CategoryViewHolder)holder).tvTitle.setText(listMovieOfCategoryAPI.get(position-1)
                    .getR()
                    .getCategory()
                    .getCategoryName());

            final MovieOfCategoryAdapter adapter = new MovieOfCategoryAdapter(mContext,
                    listMovieOfCategoryAPI
                            .get(position-1)
                            .getR()
                            .getMovies());

            GridLayoutManager layout = new GridLayoutManager(mContext, 3);
            ((CategoryViewHolder)holder).recyclerView.setHasFixedSize(true);
            ((CategoryViewHolder)holder).recyclerView.setLayoutManager(layout);
            adapter.setItemCount(6);

            ((CategoryViewHolder)holder).btShowMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapter.setItemCount(adapter.getSizeList());
                    adapter.notifyDataSetChanged();
                }
            });
            ((CategoryViewHolder)holder).recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public int getItemCount() {
        return listMovieOfCategoryAPI.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return TYPE_SLIDER;
        }else{
            return 0;
        }
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        RecyclerView recyclerView;
        TextView tvTitle;
        ImageButton btShowMore;

        public CategoryViewHolder(ViewGroup viewGroup, LayoutInflater inflater) {
            super(inflater.inflate(R.layout.layout_item_category, viewGroup, false));
            recyclerView = (RecyclerView) itemView.findViewById(R.id.recycler_view);
            tvTitle = (TextView) itemView.findViewById(R.id.title_list_movie);
            btShowMore = (ImageButton) itemView.findViewById(R.id.btn_show_more);
        }
    }


    public static class SliderViewHolder extends RecyclerView.ViewHolder {
        SliderLayout slider;

        public SliderViewHolder(ViewGroup viewGroup, LayoutInflater inflater) {
            super(inflater.inflate(R.layout.layout_item_slider, viewGroup, false));
            slider = (SliderLayout) itemView.findViewById(R.id.slider);
        }
    }

}
