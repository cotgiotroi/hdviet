
package com.ominext.hdviet.model.category.category;

import java.util.ArrayList;
import java.util.List;

public class CategoryAPI {

    private Integer e;
    private List<R> r = new ArrayList<R>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public CategoryAPI() {
    }

    /**
     * 
     * @param e
     * @param r
     */
    public CategoryAPI(Integer e, List<R> r) {
        this.e = e;
        this.r = r;
    }

    /**
     * 
     * @return
     *     The e
     */
    public Integer getE() {
        return e;
    }

    /**
     * 
     * @param e
     *     The e
     */
    public void setE(Integer e) {
        this.e = e;
    }

    /**
     * 
     * @return
     *     The r
     */
    public List<R> getR() {
        return r;
    }

    /**
     * 
     * @param r
     *     The r
     */
    public void setR(List<R> r) {
        this.r = r;
    }

}
