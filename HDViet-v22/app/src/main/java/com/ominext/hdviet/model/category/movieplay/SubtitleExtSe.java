
package com.ominext.hdviet.model.category.movieplay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubtitleExtSe {

    @SerializedName("VIE")
    @Expose
    private VIE_ vIE;
    @SerializedName("ENG")
    @Expose
    private ENG_ eNG;

    /**
     * No args constructor for use in serialization
     * 
     */
    public SubtitleExtSe() {
    }

    /**
     * 
     * @param eNG
     * @param vIE
     */
    public SubtitleExtSe(VIE_ vIE, ENG_ eNG) {
        this.vIE = vIE;
        this.eNG = eNG;
    }

    /**
     * 
     * @return
     *     The vIE
     */
    public VIE_ getVIE() {
        return vIE;
    }

    /**
     * 
     * @param vIE
     *     The VIE
     */
    public void setVIE(VIE_ vIE) {
        this.vIE = vIE;
    }

    /**
     * 
     * @return
     *     The eNG
     */
    public ENG_ getENG() {
        return eNG;
    }

    /**
     * 
     * @param eNG
     *     The ENG
     */
    public void setENG(ENG_ eNG) {
        this.eNG = eNG;
    }

}
