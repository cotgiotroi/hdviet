
package com.ominext.hdviet.model.category.movieofcategory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Thumbs {

    @SerializedName("1")
    @Expose
    private String _1;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Thumbs() {
    }

    /**
     * 
     * @param _1
     */
    public Thumbs(String _1) {
        this._1 = _1;
    }

    /**
     * 
     * @return
     *     The _1
     */
    public String get1() {
        return _1;
    }

    /**
     * 
     * @param _1
     *     The 1
     */
    public void set1(String _1) {
        this._1 = _1;
    }

}
