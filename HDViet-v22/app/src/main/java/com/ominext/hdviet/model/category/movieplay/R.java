
package com.ominext.hdviet.model.category.movieplay;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class R {

    @SerializedName("CurrentSeason")
    @Expose
    private String currentSeason;
    @SerializedName("MovieName")
    @Expose
    private String movieName;
    @SerializedName("LinkPlay")
    @Expose
    private String linkPlay;
    @SerializedName("SubtitleExt")
    @Expose
    private SubtitleExt subtitleExt;
    @SerializedName("SubtitleExtSe")
    @Expose
    private SubtitleExtSe subtitleExtSe;
    @SerializedName("Subtitle")
    @Expose
    private Subtitle subtitle;
    @SerializedName("Episode")
    @Expose
    private String episode;
    @SerializedName("AudioDub")
    @Expose
    private String audioDub;
    @SerializedName("AudioExt")
    @Expose
    private List<AudioExt> audioExt = new ArrayList<AudioExt>();
    @SerializedName("Audio")
    @Expose
    private Integer audio;
    @SerializedName("LinkPlayBackup")
    @Expose
    private Object linkPlayBackup;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Season")
    @Expose
    private List<Object> season = new ArrayList<Object>();
    @SerializedName("Adver")
    @Expose
    private List<Object> adver = new ArrayList<Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public R() {
    }

    /**
     * 
     * @param currentSeason
     * @param episode
     * @param audio
     * @param subtitleExtSe
     * @param linkPlayBackup
     * @param adver
     * @param audioExt
     * @param movieName
     * @param subtitleExt
     * @param message
     * @param audioDub
     * @param season
     * @param linkPlay
     * @param subtitle
     */
    public R(String currentSeason, String movieName, String linkPlay, SubtitleExt subtitleExt, SubtitleExtSe subtitleExtSe, Subtitle subtitle, String episode, String audioDub, List<AudioExt> audioExt, Integer audio, Object linkPlayBackup, String message, List<Object> season, List<Object> adver) {
        this.currentSeason = currentSeason;
        this.movieName = movieName;
        this.linkPlay = linkPlay;
        this.subtitleExt = subtitleExt;
        this.subtitleExtSe = subtitleExtSe;
        this.subtitle = subtitle;
        this.episode = episode;
        this.audioDub = audioDub;
        this.audioExt = audioExt;
        this.audio = audio;
        this.linkPlayBackup = linkPlayBackup;
        this.message = message;
        this.season = season;
        this.adver = adver;
    }

    /**
     * 
     * @return
     *     The currentSeason
     */
    public String getCurrentSeason() {
        return currentSeason;
    }

    /**
     * 
     * @param currentSeason
     *     The CurrentSeason
     */
    public void setCurrentSeason(String currentSeason) {
        this.currentSeason = currentSeason;
    }

    /**
     * 
     * @return
     *     The movieName
     */
    public String getMovieName() {
        return movieName;
    }

    /**
     * 
     * @param movieName
     *     The MovieName
     */
    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    /**
     * 
     * @return
     *     The linkPlay
     */
    public String getLinkPlay() {
        return linkPlay;
    }

    /**
     * 
     * @param linkPlay
     *     The LinkPlay
     */
    public void setLinkPlay(String linkPlay) {
        this.linkPlay = linkPlay;
    }

    /**
     * 
     * @return
     *     The subtitleExt
     */
    public SubtitleExt getSubtitleExt() {
        return subtitleExt;
    }

    /**
     * 
     * @param subtitleExt
     *     The SubtitleExt
     */
    public void setSubtitleExt(SubtitleExt subtitleExt) {
        this.subtitleExt = subtitleExt;
    }

    /**
     * 
     * @return
     *     The subtitleExtSe
     */
    public SubtitleExtSe getSubtitleExtSe() {
        return subtitleExtSe;
    }

    /**
     * 
     * @param subtitleExtSe
     *     The SubtitleExtSe
     */
    public void setSubtitleExtSe(SubtitleExtSe subtitleExtSe) {
        this.subtitleExtSe = subtitleExtSe;
    }

    /**
     * 
     * @return
     *     The subtitle
     */
    public Subtitle getSubtitle() {
        return subtitle;
    }

    /**
     * 
     * @param subtitle
     *     The Subtitle
     */
    public void setSubtitle(Subtitle subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * 
     * @return
     *     The episode
     */
    public String getEpisode() {
        return episode;
    }

    /**
     * 
     * @param episode
     *     The Episode
     */
    public void setEpisode(String episode) {
        this.episode = episode;
    }

    /**
     * 
     * @return
     *     The audioDub
     */
    public String getAudioDub() {
        return audioDub;
    }

    /**
     * 
     * @param audioDub
     *     The AudioDub
     */
    public void setAudioDub(String audioDub) {
        this.audioDub = audioDub;
    }

    /**
     * 
     * @return
     *     The audioExt
     */
    public List<AudioExt> getAudioExt() {
        return audioExt;
    }

    /**
     * 
     * @param audioExt
     *     The AudioExt
     */
    public void setAudioExt(List<AudioExt> audioExt) {
        this.audioExt = audioExt;
    }

    /**
     * 
     * @return
     *     The audio
     */
    public Integer getAudio() {
        return audio;
    }

    /**
     * 
     * @param audio
     *     The Audio
     */
    public void setAudio(Integer audio) {
        this.audio = audio;
    }

    /**
     * 
     * @return
     *     The linkPlayBackup
     */
    public Object getLinkPlayBackup() {
        return linkPlayBackup;
    }

    /**
     * 
     * @param linkPlayBackup
     *     The LinkPlayBackup
     */
    public void setLinkPlayBackup(Object linkPlayBackup) {
        this.linkPlayBackup = linkPlayBackup;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The Message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The season
     */
    public List<Object> getSeason() {
        return season;
    }

    /**
     * 
     * @param season
     *     The Season
     */
    public void setSeason(List<Object> season) {
        this.season = season;
    }

    /**
     * 
     * @return
     *     The adver
     */
    public List<Object> getAdver() {
        return adver;
    }

    /**
     * 
     * @param adver
     *     The Adver
     */
    public void setAdver(List<Object> adver) {
        this.adver = adver;
    }

}
