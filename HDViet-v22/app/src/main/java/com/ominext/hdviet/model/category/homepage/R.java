
package com.ominext.hdviet.model.category.homepage;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class R {

    @SerializedName("Total")
    private int total;

    @SerializedName("Movies")
    private List<Movie> movies = new ArrayList<Movie>();

    @SerializedName("Category")
    private Category_ category;

    /**
     * No args constructor for use in serialization
     * 
     */
    public R() {
    }

    /**
     * 
     * @param total
     * @param category
     * @param movies
     */
    public R(int total, List<Movie> movies, Category_ category) {
        this.total = total;
        this.movies = movies;
        this.category = category;
    }

    /**
     * 
     * @return
     *     The total
     */
    public int getTotal() {
        return total;
    }

    /**
     * 
     * @param total
     *     The Total
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * 
     * @return
     *     The movies
     */
    public List<Movie> getMovies() {
        return movies;
    }

    /**
     * 
     * @param movies
     *     The Movies
     */
    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    /**
     * 
     * @return
     *     The category
     */
    public Category_ getCategory() {
        return category;
    }

    /**
     * 
     * @param category
     *     The Category
     */
    public void setCategory(Category_ category) {
        this.category = category;
    }

}
