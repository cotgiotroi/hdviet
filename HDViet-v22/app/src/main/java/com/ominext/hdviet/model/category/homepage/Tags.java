package com.ominext.hdviet.model.category.homepage;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 20133 on 10/5/2016.
 */
public class Tags {

    @SerializedName("KeyWordID")
    private String KeyWordID;

    @SerializedName("KeyWordVN")
    private String KeyWordVN;

    @SerializedName("KeyWordEN")
    private String KeyWordEN;

    @SerializedName("KeyWordLink")
    private String KeyWordLink;

    public Tags(String keyWordID, String keyWordVN, String keyWordEN, String keyWordLink) {
        KeyWordID = keyWordID;
        KeyWordVN = keyWordVN;
        KeyWordEN = keyWordEN;
        KeyWordLink = keyWordLink;
    }

    public String getKeyWordID() {
        return KeyWordID;
    }

    public void setKeyWordID(String keyWordID) {
        KeyWordID = keyWordID;
    }

    public String getKeyWordVN() {
        return KeyWordVN;
    }

    public void setKeyWordVN(String keyWordVN) {
        KeyWordVN = keyWordVN;
    }

    public String getKeyWordEN() {
        return KeyWordEN;
    }

    public void setKeyWordEN(String keyWordEN) {
        KeyWordEN = keyWordEN;
    }

    public String getKeyWordLink() {
        return KeyWordLink;
    }

    public void setKeyWordLink(String keyWordLink) {
        KeyWordLink = keyWordLink;
    }
}
