package com.ominext.hdviet.UI.fragment;

import android.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ominext.hdviet.R;

/**
 * Created by 20133 on 9/29/2016.
 */

public class SplashFragment extends Fragment {

    private OnSplashCompleteListener listener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_splash, container, false);
        if (isNetworkConnected()) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getFragmentManager()
                            .beginTransaction()
                            .remove(SplashFragment.this)
                            .commitAllowingStateLoss();
                    listener.onComplete();
                }
            }, 1000);
        }else{
            Toast.makeText(getActivity(), "Internet is unavailable", Toast.LENGTH_SHORT).show();
        }
        return view;
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    public boolean isConnectInternet() {
        ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (conMgr.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
                || conMgr.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING) {

            return true;

        }
        return false;
    }


    public void setListener(OnSplashCompleteListener listener) {
        this.listener = listener;
    }

    public interface OnSplashCompleteListener {
        void onComplete();
    }

}
