package com.ominext.hdviet.model.category.homepage;

import com.google.gson.annotations.SerializedName;

public class HomepageAPI {

    @SerializedName("e")
    private int e;

    @SerializedName("r")
    private R r;

    /**
     * No args constructor for use in serialization
     */
    public HomepageAPI() {
    }

    /**
     * @param e
     * @param r
     */
    public HomepageAPI(int e, R r) {
        this.e = e;
        this.r = r;
    }

    /**
     * @return The e
     */
    public int getE() {
        return e;
    }

    /**
     * @param e The e
     */
    public void setE(int e) {
        this.e = e;
    }

    /**
     * @return The r
     */
    public R getR() {
        return r;
    }

    /**
     * @param r The r
     */
    public void setR(R r) {
        this.r = r;
    }

}
