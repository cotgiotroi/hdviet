package com.ominext.hdviet.model.category.search;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class R {

    @SerializedName("Total")
    private int total;

    @SerializedName("Data")
    private List<Data> data;


    public R(int total, List<Data> data) {
        this.total = total;
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }
}
