
package com.ominext.hdviet.model.category.moviedetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Thumbs {

    @SerializedName("1")
    @Expose
    private String _1;
    @SerializedName("2")
    @Expose
    private String _2;
    @SerializedName("3")
    @Expose
    private String _3;
    @SerializedName("4")
    @Expose
    private String _4;
    @SerializedName("5")
    @Expose
    private String _5;
    @SerializedName("6")
    @Expose
    private String _6;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Thumbs() {
    }

    /**
     * 
     * @param _3
     * @param _4
     * @param _5
     * @param _6
     * @param _1
     * @param _2
     */
    public Thumbs(String _1, String _2, String _3, String _4, String _5, String _6) {
        this._1 = _1;
        this._2 = _2;
        this._3 = _3;
        this._4 = _4;
        this._5 = _5;
        this._6 = _6;
    }

    /**
     * 
     * @return
     *     The _1
     */
    public String get1() {
        return _1;
    }

    /**
     * 
     * @param _1
     *     The 1
     */
    public void set1(String _1) {
        this._1 = _1;
    }

    /**
     * 
     * @return
     *     The _2
     */
    public String get2() {
        return _2;
    }

    /**
     * 
     * @param _2
     *     The 2
     */
    public void set2(String _2) {
        this._2 = _2;
    }

    /**
     * 
     * @return
     *     The _3
     */
    public String get3() {
        return _3;
    }

    /**
     * 
     * @param _3
     *     The 3
     */
    public void set3(String _3) {
        this._3 = _3;
    }

    /**
     * 
     * @return
     *     The _4
     */
    public String get4() {
        return _4;
    }

    /**
     * 
     * @param _4
     *     The 4
     */
    public void set4(String _4) {
        this._4 = _4;
    }

    /**
     * 
     * @return
     *     The _5
     */
    public String get5() {
        return _5;
    }

    /**
     * 
     * @param _5
     *     The 5
     */
    public void set5(String _5) {
        this._5 = _5;
    }

    /**
     * 
     * @return
     *     The _6
     */
    public String get6() {
        return _6;
    }

    /**
     * 
     * @param _6
     *     The 6
     */
    public void set6(String _6) {
        this._6 = _6;
    }

}
