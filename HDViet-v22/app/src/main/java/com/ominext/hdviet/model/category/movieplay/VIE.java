
package com.ominext.hdviet.model.category.movieplay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VIE {

    @SerializedName("Label")
    @Expose
    private String label;
    @SerializedName("Source")
    @Expose
    private String source;

    /**
     * No args constructor for use in serialization
     * 
     */
    public VIE() {
    }

    /**
     * 
     * @param source
     * @param label
     */
    public VIE(String label, String source) {
        this.label = label;
        this.source = source;
    }

    /**
     * 
     * @return
     *     The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * 
     * @param label
     *     The Label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * 
     * @return
     *     The source
     */
    public String getSource() {
        return source;
    }

    /**
     * 
     * @param source
     *     The Source
     */
    public void setSource(String source) {
        this.source = source;
    }

}
