
package com.ominext.hdviet.model.category.moviedetail;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class R {

    @SerializedName("MovieID")
    @Expose
    private String movieID;
    @SerializedName("MovieName")
    @Expose
    private String movieName;
    @SerializedName("KnownAs")
    @Expose
    private String knownAs;
    @SerializedName("WrapLink")
    @Expose
    private String wrapLink;
    @SerializedName("Trailer")
    @Expose
    private String trailer;

    @SerializedName("Tags")
    @Expose
    private JsonElement tags;

    @SerializedName("Category")
    @Expose
    private List<Category> category = new ArrayList<Category>();
    @SerializedName("Poster")
    @Expose
    private String poster;
    @SerializedName("Poster100x149")
    @Expose
    private String poster100x149;
    @SerializedName("Poster124x184")
    @Expose
    private String poster124x184;
    @SerializedName("Poster155x230")
    @Expose
    private String poster155x230;
    @SerializedName("Poster214x321")
    @Expose
    private String poster214x321;
    @SerializedName("Sequence")
    @Expose
    private String sequence;
    @SerializedName("CurrentSeason")
    @Expose
    private String currentSeason;
    @SerializedName("Episode")
    @Expose
    private String episode;
    @SerializedName("Runtime")
    @Expose
    private String runtime;
    @SerializedName("Cast")
    @Expose
    private String cast;
    @SerializedName("Creator")
    @Expose
    private String creator;
    @SerializedName("Director")
    @Expose
    private String director;
    @SerializedName("PlotVI")
    @Expose
    private String plotVI;
    @SerializedName("PlotEN")
    @Expose
    private String plotEN;
    @SerializedName("ImdbRating")
    @Expose
    private String imdbRating;
    @SerializedName("ImdbVotes")
    @Expose
    private Object imdbVotes;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("ReleaseDate")
    @Expose
    private String releaseDate;
    @SerializedName("NewBackdrop")
    @Expose
    private String newBackdrop;
    @SerializedName("NewBackdrop945x530")
    @Expose
    private String newBackdrop945x530;
    @SerializedName("Backdrop")
    @Expose
    private String backdrop;
    @SerializedName("Banner")
    @Expose
    private String banner;
    @SerializedName("Thumbs")
    @Expose
    private Thumbs thumbs;
    @SerializedName("AudioDub")
    @Expose
    private String audioDub;
    @SerializedName("AudioExt")
    @Expose
    private List<AudioExt> audioExt = new ArrayList<AudioExt>();
    @SerializedName("Audio")
    @Expose
    private Integer audio;
    @SerializedName("Child")
    @Expose
    private Integer child;
    @SerializedName("Season")
    @Expose
    private List<Object> season = new ArrayList<Object>();
    @SerializedName("Relative")
    @Expose
    private List<Relative> relative = new ArrayList<Relative>();
    @SerializedName("HD")
    @Expose
    private Integer hD;
    @SerializedName("Facebook_Comment")
    @Expose
    private String facebookComment;
    @SerializedName("isFavorite")
    @Expose
    private Integer isFavorite;

    /**
     * No args constructor for use in serialization
     * 
     */
    public R() {
    }

    /**
     * 
     * @param thumbs
     * @param poster100x149
     * @param newBackdrop
     * @param trailer
     * @param episode
     * @param facebookComment
     * @param imdbVotes
     * @param hD
     * @param runtime
     * @param newBackdrop945x530
     * @param plotVI
     * @param backdrop
     * @param creator
     * @param child
     * @param imdbRating
     * @param releaseDate
     * @param wrapLink
     * @param isFavorite
     * @param banner
     * @param poster124x184
     * @param tags
     * @param movieID
     * @param currentSeason
     * @param knownAs
     * @param audio
     * @param poster155x230
     * @param audioExt
     * @param director
     * @param relative
     * @param country
     * @param movieName
     * @param audioDub
     * @param category
     * @param season
     * @param cast
     * @param sequence
     * @param plotEN
     * @param poster214x321
     * @param poster
     */
    public R(String movieID, String movieName, String knownAs, String wrapLink, String trailer,JsonElement tags, List<Category> category, String poster, String poster100x149, String poster124x184, String poster155x230, String poster214x321, String sequence, String currentSeason, String episode, String runtime, String cast, String creator, String director, String plotVI, String plotEN, String imdbRating, Object imdbVotes, String country, String releaseDate, String newBackdrop, String newBackdrop945x530, String backdrop, String banner, Thumbs thumbs, String audioDub, List<AudioExt> audioExt, Integer audio, Integer child, List<Object> season, List<Relative> relative, Integer hD, String facebookComment, Integer isFavorite) {
        this.movieID = movieID;
        this.movieName = movieName;
        this.knownAs = knownAs;
        this.wrapLink = wrapLink;
        this.trailer = trailer;
        this.tags = tags;
        this.category = category;
        this.poster = poster;
        this.poster100x149 = poster100x149;
        this.poster124x184 = poster124x184;
        this.poster155x230 = poster155x230;
        this.poster214x321 = poster214x321;
        this.sequence = sequence;
        this.currentSeason = currentSeason;
        this.episode = episode;
        this.runtime = runtime;
        this.cast = cast;
        this.creator = creator;
        this.director = director;
        this.plotVI = plotVI;
        this.plotEN = plotEN;
        this.imdbRating = imdbRating;
        this.imdbVotes = imdbVotes;
        this.country = country;
        this.releaseDate = releaseDate;
        this.newBackdrop = newBackdrop;
        this.newBackdrop945x530 = newBackdrop945x530;
        this.backdrop = backdrop;
        this.banner = banner;
        this.thumbs = thumbs;
        this.audioDub = audioDub;
        this.audioExt = audioExt;
        this.audio = audio;
        this.child = child;
        this.season = season;
        this.relative = relative;
        this.hD = hD;
        this.facebookComment = facebookComment;
        this.isFavorite = isFavorite;
    }

    /**
     * 
     * @return
     *     The movieID
     */
    public String getMovieID() {
        return movieID;
    }

    /**
     * 
     * @param movieID
     *     The MovieID
     */
    public void setMovieID(String movieID) {
        this.movieID = movieID;
    }

    /**
     * 
     * @return
     *     The movieName
     */
    public String getMovieName() {
        return movieName;
    }

    /**
     * 
     * @param movieName
     *     The MovieName
     */
    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    /**
     * 
     * @return
     *     The knownAs
     */
    public String getKnownAs() {
        return knownAs;
    }

    /**
     * 
     * @param knownAs
     *     The KnownAs
     */
    public void setKnownAs(String knownAs) {
        this.knownAs = knownAs;
    }

    /**
     * 
     * @return
     *     The wrapLink
     */
    public String getWrapLink() {
        return wrapLink;
    }

    /**
     * 
     * @param wrapLink
     *     The WrapLink
     */
    public void setWrapLink(String wrapLink) {
        this.wrapLink = wrapLink;
    }

    /**
     * 
     * @return
     *     The trailer
     */
    public String getTrailer() {
        return trailer;
    }

    /**
     * 
     * @param trailer
     *     The Trailer
     */
    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    /**
     * 
     * @return
     *     The tags
     */
    public JsonElement getTags() {
        return tags;
    }

    /**
     * 
     * @param tags
     *     The Tags
     */
    public void setTags(JsonElement tags) {
        this.tags = tags;
    }

    /**
     * 
     * @return
     *     The category
     */
    public List<Category> getCategory() {
        return category;
    }

    /**
     * 
     * @param category
     *     The Category
     */
    public void setCategory(List<Category> category) {
        this.category = category;
    }

    /**
     * 
     * @return
     *     The poster
     */
    public String getPoster() {
        return poster;
    }

    /**
     * 
     * @param poster
     *     The Poster
     */
    public void setPoster(String poster) {
        this.poster = poster;
    }

    /**
     * 
     * @return
     *     The poster100x149
     */
    public String getPoster100x149() {
        return poster100x149;
    }

    /**
     * 
     * @param poster100x149
     *     The Poster100x149
     */
    public void setPoster100x149(String poster100x149) {
        this.poster100x149 = poster100x149;
    }

    /**
     * 
     * @return
     *     The poster124x184
     */
    public String getPoster124x184() {
        return poster124x184;
    }

    /**
     * 
     * @param poster124x184
     *     The Poster124x184
     */
    public void setPoster124x184(String poster124x184) {
        this.poster124x184 = poster124x184;
    }

    /**
     * 
     * @return
     *     The poster155x230
     */
    public String getPoster155x230() {
        return poster155x230;
    }

    /**
     * 
     * @param poster155x230
     *     The Poster155x230
     */
    public void setPoster155x230(String poster155x230) {
        this.poster155x230 = poster155x230;
    }

    /**
     * 
     * @return
     *     The poster214x321
     */
    public String getPoster214x321() {
        return poster214x321;
    }

    /**
     * 
     * @param poster214x321
     *     The Poster214x321
     */
    public void setPoster214x321(String poster214x321) {
        this.poster214x321 = poster214x321;
    }

    /**
     * 
     * @return
     *     The sequence
     */
    public String getSequence() {
        return sequence;
    }

    /**
     * 
     * @param sequence
     *     The Sequence
     */
    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    /**
     * 
     * @return
     *     The currentSeason
     */
    public String getCurrentSeason() {
        return currentSeason;
    }

    /**
     * 
     * @param currentSeason
     *     The CurrentSeason
     */
    public void setCurrentSeason(String currentSeason) {
        this.currentSeason = currentSeason;
    }

    /**
     * 
     * @return
     *     The episode
     */
    public String getEpisode() {
        return episode;
    }

    /**
     * 
     * @param episode
     *     The Episode
     */
    public void setEpisode(String episode) {
        this.episode = episode;
    }

    /**
     * 
     * @return
     *     The runtime
     */
    public String getRuntime() {
        return runtime;
    }

    /**
     * 
     * @param runtime
     *     The Runtime
     */
    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    /**
     * 
     * @return
     *     The cast
     */
    public String getCast() {
        return cast;
    }

    /**
     * 
     * @param cast
     *     The Cast
     */
    public void setCast(String cast) {
        this.cast = cast;
    }

    /**
     * 
     * @return
     *     The creator
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 
     * @param creator
     *     The Creator
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 
     * @return
     *     The director
     */
    public String getDirector() {
        return director;
    }

    /**
     * 
     * @param director
     *     The Director
     */
    public void setDirector(String director) {
        this.director = director;
    }

    /**
     * 
     * @return
     *     The plotVI
     */
    public String getPlotVI() {
        return plotVI;
    }

    /**
     * 
     * @param plotVI
     *     The PlotVI
     */
    public void setPlotVI(String plotVI) {
        this.plotVI = plotVI;
    }

    /**
     * 
     * @return
     *     The plotEN
     */
    public String getPlotEN() {
        return plotEN;
    }

    /**
     * 
     * @param plotEN
     *     The PlotEN
     */
    public void setPlotEN(String plotEN) {
        this.plotEN = plotEN;
    }

    /**
     * 
     * @return
     *     The imdbRating
     */
    public String getImdbRating() {
        return imdbRating;
    }

    /**
     * 
     * @param imdbRating
     *     The ImdbRating
     */
    public void setImdbRating(String imdbRating) {
        this.imdbRating = imdbRating;
    }

    /**
     * 
     * @return
     *     The imdbVotes
     */
    public Object getImdbVotes() {
        return imdbVotes;
    }

    /**
     * 
     * @param imdbVotes
     *     The ImdbVotes
     */
    public void setImdbVotes(Object imdbVotes) {
        this.imdbVotes = imdbVotes;
    }

    /**
     * 
     * @return
     *     The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The Country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * 
     * @return
     *     The releaseDate
     */
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     * 
     * @param releaseDate
     *     The ReleaseDate
     */
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * 
     * @return
     *     The newBackdrop
     */
    public String getNewBackdrop() {
        return newBackdrop;
    }

    /**
     * 
     * @param newBackdrop
     *     The NewBackdrop
     */
    public void setNewBackdrop(String newBackdrop) {
        this.newBackdrop = newBackdrop;
    }

    /**
     * 
     * @return
     *     The newBackdrop945x530
     */
    public String getNewBackdrop945x530() {
        return newBackdrop945x530;
    }

    /**
     * 
     * @param newBackdrop945x530
     *     The NewBackdrop945x530
     */
    public void setNewBackdrop945x530(String newBackdrop945x530) {
        this.newBackdrop945x530 = newBackdrop945x530;
    }

    /**
     * 
     * @return
     *     The backdrop
     */
    public String getBackdrop() {
        return backdrop;
    }

    /**
     * 
     * @param backdrop
     *     The Backdrop
     */
    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    /**
     * 
     * @return
     *     The banner
     */
    public String getBanner() {
        return banner;
    }

    /**
     * 
     * @param banner
     *     The Banner
     */
    public void setBanner(String banner) {
        this.banner = banner;
    }

    /**
     * 
     * @return
     *     The thumbs
     */
    public Thumbs getThumbs() {
        return thumbs;
    }

    /**
     * 
     * @param thumbs
     *     The Thumbs
     */
    public void setThumbs(Thumbs thumbs) {
        this.thumbs = thumbs;
    }

    /**
     * 
     * @return
     *     The audioDub
     */
    public String getAudioDub() {
        return audioDub;
    }

    /**
     * 
     * @param audioDub
     *     The AudioDub
     */
    public void setAudioDub(String audioDub) {
        this.audioDub = audioDub;
    }

    /**
     * 
     * @return
     *     The audioExt
     */
    public List<AudioExt> getAudioExt() {
        return audioExt;
    }

    /**
     * 
     * @param audioExt
     *     The AudioExt
     */
    public void setAudioExt(List<AudioExt> audioExt) {
        this.audioExt = audioExt;
    }

    /**
     * 
     * @return
     *     The audio
     */
    public Integer getAudio() {
        return audio;
    }

    /**
     * 
     * @param audio
     *     The Audio
     */
    public void setAudio(Integer audio) {
        this.audio = audio;
    }

    /**
     * 
     * @return
     *     The child
     */
    public Integer getChild() {
        return child;
    }

    /**
     * 
     * @param child
     *     The Child
     */
    public void setChild(Integer child) {
        this.child = child;
    }

    /**
     * 
     * @return
     *     The season
     */
    public List<Object> getSeason() {
        return season;
    }

    /**
     * 
     * @param season
     *     The Season
     */
    public void setSeason(List<Object> season) {
        this.season = season;
    }

    /**
     * 
     * @return
     *     The relative
     */
    public List<Relative> getRelative() {
        return relative;
    }

    /**
     * 
     * @param relative
     *     The Relative
     */
    public void setRelative(List<Relative> relative) {
        this.relative = relative;
    }

    /**
     * 
     * @return
     *     The hD
     */
    public Integer getHD() {
        return hD;
    }

    /**
     * 
     * @param hD
     *     The HD
     */
    public void setHD(Integer hD) {
        this.hD = hD;
    }

    /**
     * 
     * @return
     *     The facebookComment
     */
    public String getFacebookComment() {
        return facebookComment;
    }

    /**
     * 
     * @param facebookComment
     *     The Facebook_Comment
     */
    public void setFacebookComment(String facebookComment) {
        this.facebookComment = facebookComment;
    }

    /**
     * 
     * @return
     *     The isFavorite
     */
    public Integer getIsFavorite() {
        return isFavorite;
    }

    /**
     * 
     * @param isFavorite
     *     The isFavorite
     */
    public void setIsFavorite(Integer isFavorite) {
        this.isFavorite = isFavorite;
    }

}
