
package com.ominext.hdviet.model.category.homepage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AudioExt {

    @SerializedName("Index")
    @Expose
    private String index;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Label")
    @Expose
    private String label;
    @SerializedName("Default")
    @Expose
    private String _default;

    /**
     * No args constructor for use in serialization
     * 
     */
    public AudioExt() {
    }

    /**
     * 
     * @param index
     * @param _default
     * @param label
     * @param type
     */
    public AudioExt(String index, String type, String label, String _default) {
        this.index = index;
        this.type = type;
        this.label = label;
        this._default = _default;
    }

    /**
     * 
     * @return
     *     The index
     */
    public String getIndex() {
        return index;
    }

    /**
     * 
     * @param index
     *     The Index
     */
    public void setIndex(String index) {
        this.index = index;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The Type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * 
     * @param label
     *     The Label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * 
     * @return
     *     The _default
     */
    public String getDefault() {
        return _default;
    }

    /**
     * 
     * @param _default
     *     The Default
     */
    public void setDefault(String _default) {
        this._default = _default;
    }

}
