package com.ominext.hdviet.UI.activity;

import android.os.Bundle;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.ominext.hdviet.R;

import static com.ominext.hdviet.api.Config.KEY_YOUTUBE_API;

/**
 * Created by 20133 on 10/8/2016.
 */

public class YoutubeActivity extends YouTubeBaseActivity {
    private YouTubePlayerView youTubePlayerView;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_youtube);

        String linkTrailer = getIntent().getStringExtra(DetailActivity.KEY_VIDEO);
        final String keyVideo = linkTrailer.substring(linkTrailer.indexOf("v=")+2);

        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtubeAPI);
        youTubePlayerView.initialize(KEY_YOUTUBE_API, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(keyVideo);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        });
    }
}
