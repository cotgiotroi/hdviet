package com.ominext.hdviet.api;

import com.ominext.hdviet.model.category.category.CategoryAPI;
import com.ominext.hdviet.model.category.homepage.HomepageAPI;
import com.ominext.hdviet.model.category.moviedetail.MovieDetailAPI;
import com.ominext.hdviet.model.category.movieofcategory.MovieOfCategoryAPI;
import com.ominext.hdviet.model.category.movieplay.MoviePlayAPI;
import com.ominext.hdviet.model.category.search.SearchAPI;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

import static com.ominext.hdviet.api.Config.PATH_GET_DATA_CATEGORY;
import static com.ominext.hdviet.api.Config.PATH_GET_DATA_HOMEPAGE;
import static com.ominext.hdviet.api.Config.PATH_GET_DATA_MOVIE_DETAIL;
import static com.ominext.hdviet.api.Config.PATH_GET_DATA_MOVIE_OF_CATEGORY;
import static com.ominext.hdviet.api.Config.PATH_GET_DATA_MOVIE_PLAY;
import static com.ominext.hdviet.api.Config.PATH_GET_DATA_SEARCH;

/**
 * Created by 20133 on 10/4/2016.
 */

public interface HDVietAPI {

    @GET(PATH_GET_DATA_CATEGORY)
    Call<CategoryAPI> getDataCategory(@QueryMap HashMap<String, String> params);

    @GET(PATH_GET_DATA_HOMEPAGE)
    Call<HomepageAPI> getDataHomepage(@QueryMap HashMap<String, String> params);

    @GET(PATH_GET_DATA_MOVIE_DETAIL)
    Call<MovieDetailAPI> getDataMovieDetail(@QueryMap HashMap<String, String> params);

    @GET(PATH_GET_DATA_MOVIE_PLAY)
    Call<MoviePlayAPI> getDataMoviePlay(@QueryMap HashMap<String, String> params);

    @GET(PATH_GET_DATA_SEARCH)
    Call<SearchAPI> getDataSearch(@QueryMap HashMap<String, String> params);

    @GET(PATH_GET_DATA_MOVIE_OF_CATEGORY)
    Call<MovieOfCategoryAPI> getDataMovieOfCategory(@QueryMap HashMap<String, String> params);
}
