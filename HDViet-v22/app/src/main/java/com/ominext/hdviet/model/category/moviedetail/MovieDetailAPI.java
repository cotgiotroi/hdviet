
package com.ominext.hdviet.model.category.moviedetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MovieDetailAPI {

    @SerializedName("e")
    @Expose
    private Integer e;
    @SerializedName("r")
    @Expose
    private R r;

    /**
     * No args constructor for use in serialization
     * 
     */
    public MovieDetailAPI() {
    }

    /**
     * 
     * @param e
     * @param r
     */
    public MovieDetailAPI(Integer e, R r) {
        this.e = e;
        this.r = r;
    }

    /**
     * 
     * @return
     *     The e
     */
    public Integer getE() {
        return e;
    }

    /**
     * 
     * @param e
     *     The e
     */
    public void setE(Integer e) {
        this.e = e;
    }

    /**
     * 
     * @return
     *     The r
     */
    public R getR() {
        return r;
    }

    /**
     * 
     * @param r
     *     The r
     */
    public void setR(R r) {
        this.r = r;
    }

}
