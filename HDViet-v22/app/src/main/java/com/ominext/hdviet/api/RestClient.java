package com.ominext.hdviet.api;

import android.util.Log;

import com.ominext.hdviet.model.category.category.CategoryAPI;
import com.ominext.hdviet.model.category.homepage.HomepageAPI;
import com.ominext.hdviet.model.category.moviedetail.MovieDetailAPI;
import com.ominext.hdviet.model.category.movieofcategory.MovieOfCategoryAPI;
import com.ominext.hdviet.model.category.movieplay.MoviePlayAPI;
import com.ominext.hdviet.model.category.search.SearchAPI;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ominext.hdviet.api.Config.API_BASE_URL;
import static com.ominext.hdviet.api.Config.SIGN_KEY;

/**
 * Created by 20133 on 10/4/2016.
 */

public class RestClient {
    public static final String TAG = "RestClient";

    private OnRestClientLoadDataCompleteListener loadDataComplete;

    private HDVietAPI mHDVietAPI;

    public RestClient() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mHDVietAPI = retrofit.create(HDVietAPI.class);

    }


    /***
     * null
     *
     * @return CategoryAPI
     */
    public void getListCategory() {
        HashMap<String, String> params = new HashMap<>();
        params.put("sign", SIGN_KEY);

        Call<CategoryAPI> categoryData = mHDVietAPI.getDataCategory(params);
        categoryData.enqueue(new Callback<CategoryAPI>() {
            @Override
            public void onResponse(Call<CategoryAPI> call, Response<CategoryAPI> response) {
                if (response.body() != null) {
                    CategoryAPI categoryAPI = response.body();
                    /*Log.i(TAG, "SIZE CATEGORY LIST: " + categoryAPI.getR().size());
                    for (int i = 0; i < categoryAPI.getR().size(); i++) {
                        Log.i(TAG, categoryAPI.getR().get(i).toString());
                    }*/
                    loadDataComplete.onLoadCategoryComplete(categoryAPI);
                }
            }

            @Override
            public void onFailure(Call<CategoryAPI> call, Throwable t) {
                Log.i(TAG, "" + t.toString());
            }
        });
    }


    /***
     * null
     *
     * @return HomepageAPI
     */
    public void getHomepageData() {

        HashMap<String, String> params = new HashMap<>();
        params.put("sign", SIGN_KEY);

        Call<HomepageAPI> homepageData = mHDVietAPI.getDataHomepage(params);
        homepageData.enqueue(new Callback<HomepageAPI>() {
            @Override
            public void onResponse(Call<HomepageAPI> call, Response<HomepageAPI> response) {
                if (response.body() != null) {
                    HomepageAPI homepageAPI = response.body();
                    /*Log.i(TAG, "=========  SIZE MOVIE LIST IN HOMEPAGE: " + homepageAPI.getR().getMovies().size());
                    for (int i = 0; i < homepageAPI.getR().getMovies().size(); i++) {
                        Movie movie = homepageAPI.getR().getMovies().get(i);
                        if (movie.getTags().isJsonArray()) {
                            Log.i(TAG, movie.getMovieName() + " : is JsonArray ");
                        } else if (movie.getTags().toString().equals("false")) {
                            Log.i(TAG, movie.getMovieName() + " : is JsonObject: " + movie.getTags());
                        }
                    }
                    Log.i(TAG, "----------------------------------------------------------------");*/
                    loadDataComplete.onLoadHomepageComplete(homepageAPI);
                }
            }

            @Override
            public void onFailure(Call<HomepageAPI> call, Throwable t) {
                Log.i(TAG, "" + t.toString());
            }
        });
    }


    /***
     * @param movieid : movieid
     * @param ep      : tập
     * @return MovieDetailAPI
     */
    public void getMovieDetailData(String movieid, int ep) {

        HashMap<String, String> params = new HashMap<>();
        params.put("movieid", movieid);
        params.put("ep", ep + "");
        params.put("sign", SIGN_KEY);

        Call<MovieDetailAPI> movieDetailData = mHDVietAPI.getDataMovieDetail(params);
        movieDetailData.enqueue(new Callback<MovieDetailAPI>() {
            @Override
            public void onResponse(Call<MovieDetailAPI> call, Response<MovieDetailAPI> response) {
                if (response.body() != null) {
                    MovieDetailAPI movieDetailAPI = response.body();
                    /*Log.i(TAG, "=========MOVIE DETAIL API:");
                    Log.i(TAG, movieDetailAPI.getR().getMovieName());
                    Log.i(TAG, "----------------------------------------------------------------");*/
                    loadDataComplete.onLoadMovieDetailComplete(movieDetailAPI);
                }
            }

            @Override
            public void onFailure(Call<MovieDetailAPI> call, Throwable t) {
                Log.i(TAG, "" + t.toString());
            }
        });
    }


    /***
     * @param movieid
     * @param ep      : tập
     * @return MoviePlayAPI
     */
    public void getMoviePlayData(int movieid, int ep) {

        HashMap<String, String> params = new HashMap<>();
        params.put("movieid", movieid + "");
        params.put("ep", ep + "");
        params.put("sign", SIGN_KEY);

        Call<MoviePlayAPI> moviePlayData = mHDVietAPI.getDataMoviePlay(params);
        moviePlayData.enqueue(new Callback<MoviePlayAPI>() {
            @Override
            public void onResponse(Call<MoviePlayAPI> call, Response<MoviePlayAPI> response) {
                if (response.body() != null) {
                    MoviePlayAPI moviePlayAPI = response.body();

                    /*Log.i(TAG, "=========MOVIE PLAY DATA: " + moviePlayAPI.getR().getMovieName());
                    Log.i(TAG, "----------------------------------------------------------------");*/
                    loadDataComplete.onLoadMoviePlayComplete(moviePlayAPI);
                }
            }

            @Override
            public void onFailure(Call<MoviePlayAPI> call, Throwable t) {
                Log.i(TAG, "" + t.toString());
            }
        });
    }


    /***
     * @param categoryId : Id của Category
     * @param key        : vị trí load
     * @return SearchAPI
     */
    public void getMovieOfCategoryData(String categoryId, String key) {

        HashMap<String, String> params = new HashMap<>();
        params.put("categoryId", categoryId);
        params.put("key", key);
        params.put("sign", SIGN_KEY);

        Call<MovieOfCategoryAPI> movieOfCategoryData = mHDVietAPI.getDataMovieOfCategory(params);
        movieOfCategoryData.enqueue(new Callback<MovieOfCategoryAPI>() {
            @Override
            public void onResponse(Call<MovieOfCategoryAPI> call, Response<MovieOfCategoryAPI> response) {
                if (response.body() != null) {
                    MovieOfCategoryAPI movieOfCategoryAPI = response.body();

                    /*Log.i(TAG, "=========SIZE MOVIE OF CATEGORY: " + movieOfCategoryAPI.getR().getMovies().size());
                    for (int i = 0; i < movieOfCategoryAPI.getR().getMovies().size(); i++) {
                        Log.i(TAG, movieOfCategoryAPI.getR().getMovies().get(i).toString());
                    }
                    Log.i(TAG, "----------------------------------------------------------------");*/
                    loadDataComplete.onLoadMovieOfCategoryComplete(movieOfCategoryAPI);
                }
            }

            @Override
            public void onFailure(Call<MovieOfCategoryAPI> call, Throwable t) {
                Log.i(TAG, "" + t.toString());
            }
        });
    }


    /***
     * @param key : key tìm kiếm
     * @return MovieOfCategoryAPI
     */
    public void getSearchData(String key) {

        HashMap<String, String> params = new HashMap<>();
        params.put("key", key);
        params.put("sign", SIGN_KEY);


        Call<SearchAPI> searchData = mHDVietAPI.getDataSearch(params);
        searchData.enqueue(new Callback<SearchAPI>() {
            @Override
            public void onResponse(Call<SearchAPI> call, Response<SearchAPI> response) {
                if (response.body() != null) {
                    SearchAPI searchAPI = response.body();

                    /*Log.i(TAG, "=========SIZE CATEGORY SEARCH LIST: " + searchAPI.getR().get(0).getData().size());
                    for (int i = 0; i < searchAPI.getR().get(0).getData().size(); i++) {
                        Log.i(TAG, searchAPI.getR().get(0).getData().get(i).toString());
                    }
                    Log.i(TAG, "----------------------------------------------------------------");*/
                    loadDataComplete.onLoadDataSearchComplete(searchAPI);
                }
            }

            @Override
            public void onFailure(Call<SearchAPI> call, Throwable t) {
                Log.i(TAG, "" + t.toString());
            }
        });
    }

    public void setLoadDataComplete(OnRestClientLoadDataCompleteListener loadDataComplete) {
        this.loadDataComplete = loadDataComplete;
    }
}
