package com.ominext.hdviet.UI.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.ominext.hdviet.R;
import com.ominext.hdviet.adapter.MovieSavedAdapter;

/**
 * Created by 20133 on 10/21/2016.
 */

public class SavedMovieActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_saved);
        initViews();
        initToolBar();
    }

    private void initViews() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_saved);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new MovieSavedAdapter(this));
    }

    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_saved);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setTitle("Phim đã lưu");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
