
package com.ominext.hdviet.model.category.search;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchAPI {

    @SerializedName("e")
    private int e;

    @SerializedName("r")
    private List<R> r;

    public SearchAPI(int e, List<R> r) {
        this.e = e;
        this.r = r;
    }

    public int getE() {
        return e;
    }

    public void setE(int e) {
        this.e = e;
    }

    public List<R> getR() {
        return r;
    }

    public void setR(List<R> r) {
        this.r = r;
    }
}
