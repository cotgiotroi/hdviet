package com.ominext.hdviet.UI.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.ominext.hdviet.R;
import com.ominext.hdviet.adapter.MovieOfCategoryAdapter;
import com.ominext.hdviet.api.OnRestClientLoadDataCompleteListener;
import com.ominext.hdviet.api.RestClient;
import com.ominext.hdviet.model.category.category.CategoryAPI;
import com.ominext.hdviet.model.category.homepage.HomepageAPI;
import com.ominext.hdviet.model.category.moviedetail.MovieDetailAPI;
import com.ominext.hdviet.model.category.movieofcategory.MovieOfCategoryAPI;
import com.ominext.hdviet.model.category.movieplay.MoviePlayAPI;
import com.ominext.hdviet.model.category.search.SearchAPI;

/**
 * Created by 20133 on 10/12/2016.
 */

public class CategoryActivity extends AppCompatActivity implements OnRestClientLoadDataCompleteListener {

    private Toolbar mToolbar;
    private ProgressDialog dialog;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        showDialog();
        initViews();
    }

    private void showDialog() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("loading...");
        dialog.setCancelable(true);
        dialog.show();
    }

    private void dismissDialog() {
        dialog.dismiss();
    }
    private void initViews() {
        getData();
        initToolBar();
        initRecyclerView();
    }

    private void getData() {
        RestClient client = new RestClient();
        client.setLoadDataComplete(this);
        String categoryId = getIntent().getStringExtra(MainActivity.CATEGORY_ID);
        if(Integer.valueOf(categoryId)<0){
            client.getHomepageData();
        }else{
            client.getMovieOfCategoryData(categoryId,"phim");
        }
    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_category);
    }

    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_category);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onLoadHomepageComplete(HomepageAPI homepageAPI) {
        dismissDialog();
        mToolbar.setTitle(homepageAPI.getR().getCategory().getCategoryName());
        MovieOfCategoryAdapter adapter = new MovieOfCategoryAdapter(CategoryActivity.this,homepageAPI.getR().getMovies(),-1);
        recyclerView.setLayoutManager(new GridLayoutManager(CategoryActivity.this,2));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onLoadMovieOfCategoryComplete(MovieOfCategoryAPI movieOfCategoryAPI) {
        dismissDialog();
        mToolbar.setTitle(movieOfCategoryAPI.getR().getCategory().getCategoryName());
        MovieOfCategoryAdapter adapter = new MovieOfCategoryAdapter(CategoryActivity.this,movieOfCategoryAPI.getR().getMovies());
        recyclerView.setLayoutManager(new GridLayoutManager(CategoryActivity.this,2));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onLoadMoviePlayComplete(MoviePlayAPI moviePlayAPI) {

    }

    @Override
    public void onLoadDataSearchComplete(SearchAPI searchAPI) {

    }


    @Override
    public void onLoadCategoryComplete(CategoryAPI categoryAPI) {

    }

    @Override
    public void onLoadMovieDetailComplete(MovieDetailAPI movieDetailAPI) {

    }
}
