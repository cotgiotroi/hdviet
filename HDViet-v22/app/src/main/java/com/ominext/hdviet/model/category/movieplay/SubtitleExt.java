
package com.ominext.hdviet.model.category.movieplay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubtitleExt {

    @SerializedName("VIE")
    @Expose
    private VIE vIE;
    @SerializedName("ENG")
    @Expose
    private ENG eNG;

    /**
     * No args constructor for use in serialization
     * 
     */
    public SubtitleExt() {
    }

    /**
     * 
     * @param eNG
     * @param vIE
     */
    public SubtitleExt(VIE vIE, ENG eNG) {
        this.vIE = vIE;
        this.eNG = eNG;
    }

    /**
     * 
     * @return
     *     The vIE
     */
    public VIE getVIE() {
        return vIE;
    }

    /**
     * 
     * @param vIE
     *     The VIE
     */
    public void setVIE(VIE vIE) {
        this.vIE = vIE;
    }

    /**
     * 
     * @return
     *     The eNG
     */
    public ENG getENG() {
        return eNG;
    }

    /**
     * 
     * @param eNG
     *     The ENG
     */
    public void setENG(ENG eNG) {
        this.eNG = eNG;
    }

}
