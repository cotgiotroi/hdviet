package com.ominext.hdviet.UI.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.ominext.hdviet.R;
import com.ominext.hdviet.UI.ViewUtils;
import com.ominext.hdviet.UI.fragment.SplashFragment;
import com.ominext.hdviet.adapter.ContentAdapter;
import com.ominext.hdviet.adapter.MovieSearchAdapter;
import com.ominext.hdviet.api.OnRestClientLoadDataCompleteListener;
import com.ominext.hdviet.api.RestClient;
import com.ominext.hdviet.model.category.category.CategoryAPI;
import com.ominext.hdviet.model.category.homepage.HomepageAPI;
import com.ominext.hdviet.model.category.moviedetail.MovieDetailAPI;
import com.ominext.hdviet.model.category.movieofcategory.MovieOfCategoryAPI;
import com.ominext.hdviet.model.category.movieplay.MoviePlayAPI;
import com.ominext.hdviet.model.category.search.Data;
import com.ominext.hdviet.model.category.search.SearchAPI;

import java.util.ArrayList;
import java.util.List;

import static com.ominext.hdviet.adapter.MovieOfCategoryAdapter.MOVIE_ID;

public class MainActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener, NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener, BaseSliderView.OnSliderClickListener, OnRestClientLoadDataCompleteListener, SplashFragment.OnSplashCompleteListener, View.OnClickListener {
    public static final String CATEGORY_ID = "CATEGORY_ID";

    private Toolbar mToolbar;
    private ImageView imgDismiss;
    private DrawerLayout mDrawer;
    private SearchView searchView;
    private ProgressDialog dialog;
    private MenuItem mPreviousItem;
    private NavigationView mNaviView;
    private LinearLayout layoutSearchPopup;
    private RecyclerView recyclerViewSearch;
    private RecyclerView recyclerViewContent;

    private RestClient client;
    private HomepageAPI mHomepageAPI;
    private CategoryAPI mCategoryAPI;
    private MovieSearchAdapter adapterSearch;
    private List<Data> movieList = new ArrayList<>();
    private MovieOfCategoryAPI mPreviousMovieOfCategoryAPI;
    private List<MovieOfCategoryAPI> movieOfCategoryAPIList = new ArrayList<>();

    private boolean isFirst = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSplash();
    }


    private void setSplash() {
        SplashFragment splash = new SplashFragment();
        splash.setListener(this);
        getFragmentManager()
                .beginTransaction()
                .add(R.id.layout_main, splash)
                .commitAllowingStateLoss();
    }

    @Override
    public void onComplete() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        client = new RestClient();
        client.getListCategory();
        client.setLoadDataComplete(this);
    }

    private void initViews() {

        recyclerViewContent = (RecyclerView) findViewById(R.id.recycler_view_content);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer);
        imgDismiss = (ImageView) findViewById(R.id.img_dismiss);
        searchView = (SearchView) findViewById(R.id.search_popup);
        layoutSearchPopup = (LinearLayout) findViewById(R.id.layout_search_parent);
        recyclerViewSearch = (RecyclerView) findViewById(R.id.recycler_view_search);

        mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        mNaviView = (NavigationView) findViewById(R.id.navi);
        mNaviView.setNavigationItemSelectedListener(this);

        searchView.setOnQueryTextListener(this);
        imgDismiss.setOnClickListener(this);
    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(mToolbar);
        mToolbar.setOnMenuItemClickListener(this);
        mToolbar.setTitle("");

        // Adding menu icon to Toolbar
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowCustomEnabled(true);
            VectorDrawableCompat indicator = VectorDrawableCompat.create(getResources(), R.drawable.ic_menu, getTheme());
            supportActionBar.setHomeAsUpIndicator(indicator);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initNavigation() {
        Menu menu = mNaviView.getMenu();

        int sizeCategory = mCategoryAPI.getR().size();

        for (int i = 0; i < sizeCategory; i++) {
            com.ominext.hdviet.model.category.category.R r = mCategoryAPI.getR().get(i);
            menu.add(R.id.type_movie, Integer.valueOf(r.getCategoryID()),
                    Menu.NONE,
                    r.getCategoryName())
                .setIcon(R.drawable.ic_videocam_24dp);
        }
    }

    private void initCategory() {

        ContentAdapter adapter = new ContentAdapter(movieOfCategoryAPIList, this);
        recyclerViewContent.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewContent.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_dismiss:
                Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.translate_up);
                layoutSearchPopup.startAnimation(animation);
                layoutSearchPopup.setVisibility(View.INVISIBLE);
                searchView.setQuery(null, false);
                break;
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search) {
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.translate_down);
            layoutSearchPopup.setVisibility(View.VISIBLE);
            layoutSearchPopup.startAnimation(animation);
            searchView.requestFocus();
            ViewUtils.showKeyboard(MainActivity.this);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            mDrawer.openDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    public boolean onNavigationItemSelected(MenuItem item) {
        Toast.makeText(this, "" + item.getTitle(), Toast.LENGTH_SHORT).show();
        if(item.getItemId() == R.id.phim_da_luu){
            startActivity(new Intent(MainActivity.this,SavedMovieActivity.class));
            mDrawer.closeDrawers();
            return true;
        }
        Intent intent = new Intent(MainActivity.this, CategoryActivity.class);
        intent.putExtra(CATEGORY_ID, item.getItemId() + "");
        MainActivity.this.startActivity(intent);
        item.setChecked(true);
        if (mPreviousItem != null && mPreviousItem.getItemId() != item.getItemId()) {
            mPreviousItem.setChecked(false);
        }
        mPreviousItem = item;
        mDrawer.closeDrawers();
        return true;
    }


    @Override
    public void onBackPressed() {
        if (mDrawer != null && mDrawer.isDrawerOpen(Gravity.LEFT)) {
            mDrawer.closeDrawer(Gravity.LEFT);
        } else if (layoutSearchPopup.getVisibility() == View.VISIBLE) {
            searchView.setQuery(null, false);
            layoutSearchPopup.setVisibility(View.INVISIBLE);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onLoadCategoryComplete(CategoryAPI categoryAPI) {
        mCategoryAPI = categoryAPI;
        for (int i = 0; i < categoryAPI.getR().size() && i < 6; i++) {
            client.getMovieOfCategoryData(categoryAPI.getR().get(i).getCategoryID() + "", "phim");
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if ("".equals(newText)) {
            movieList.clear();
        } else if (newText.length() >= 3) {
            client.getSearchData(newText);
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onLoadHomepageComplete(HomepageAPI homepageAPI) {
        mHomepageAPI = homepageAPI;
    }

    @Override
    public void onLoadMovieDetailComplete(MovieDetailAPI movieDetailAPI) {
    }

    @Override
    public void onLoadMovieOfCategoryComplete(MovieOfCategoryAPI movieOfCategoryAPI) {
        if (movieOfCategoryAPI != mPreviousMovieOfCategoryAPI) {
            movieOfCategoryAPIList.add(movieOfCategoryAPI);
            mPreviousMovieOfCategoryAPI = movieOfCategoryAPI;
        }
        if (isFirst) {
            initViews();
            initToolbar();
            initNavigation();
            isFirst = false;
        }
        initCategory();
        dialog.dismiss();
    }

    @Override
    public void onLoadMoviePlayComplete(MoviePlayAPI moviePlayAPI) {
        // do notthing
    }

    @Override
    public void onLoadDataSearchComplete(SearchAPI searchAPI) {
        movieList.clear();
        if (searchAPI.getR().get(0) != null) {
            movieList.addAll(searchAPI.getR().get(0).getData());
        }
        adapterSearch = new MovieSearchAdapter(movieList, this);
        recyclerViewSearch.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recyclerViewSearch.setHasFixedSize(true);
        recyclerViewSearch.setAdapter(adapterSearch);
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {
        String idMovie = slider.getBundle().get(MOVIE_ID).toString();
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra(MOVIE_ID, idMovie);
        MainActivity.this.startActivity(intent);
    }

}
