package com.ominext.hdviet.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ominext.hdviet.R;
import com.ominext.hdviet.UI.ViewUtils;
import com.ominext.hdviet.UI.activity.DetailActivity;
import com.ominext.hdviet.model.category.search.Data;

import java.util.List;

import static com.ominext.hdviet.adapter.MovieOfCategoryAdapter.MOVIE_ID;

/**
 * Created by 20133 on 10/7/2016.
 */

public class MovieSearchAdapter extends RecyclerView.Adapter<MovieSearchAdapter.ViewHolder> {

    private Context mContext;
    private List<Data> moviesFound;

    public MovieSearchAdapter(List<Data> arrMovies, Context mContext) {
        this.moviesFound = arrMovies;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(parent, LayoutInflater.from(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tvKnowAss.setText(moviesFound.get(position).getKnownAs());
        holder.tvMovieName.setText(moviesFound.get(position).getMovieName());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.hideSoftKeyboard((Activity) mContext);
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra(MOVIE_ID, moviesFound.get(position).getMovieID());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesFound.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvKnowAss;
        LinearLayout layout;
        TextView tvMovieName;
        ImageView imgViewDetail;

        public ViewHolder(ViewGroup viewGroup, LayoutInflater inflater) {
            super(inflater.inflate(R.layout.layout_item_search, viewGroup, false));

            tvKnowAss = (TextView) itemView.findViewById(R.id.tv_know_as);
            layout = (LinearLayout) itemView.findViewById(R.id.linear_search);
            tvMovieName = (TextView) itemView.findViewById(R.id.tv_movie_name);
            imgViewDetail = (ImageView) itemView.findViewById(R.id.img_view_detail);
        }
    }
}
