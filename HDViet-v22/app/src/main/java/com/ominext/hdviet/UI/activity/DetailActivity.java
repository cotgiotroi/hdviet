package com.ominext.hdviet.UI.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ominext.hdviet.R;
import com.ominext.hdviet.adapter.MovieRelativeAdapter;
import com.ominext.hdviet.api.OnRestClientLoadDataCompleteListener;
import com.ominext.hdviet.api.RestClient;
import com.ominext.hdviet.model.category.category.CategoryAPI;
import com.ominext.hdviet.model.category.homepage.HomepageAPI;
import com.ominext.hdviet.model.category.moviedetail.MovieDetailAPI;
import com.ominext.hdviet.model.category.movieofcategory.MovieOfCategoryAPI;
import com.ominext.hdviet.model.category.movieplay.MoviePlayAPI;
import com.ominext.hdviet.model.category.search.SearchAPI;
import com.squareup.picasso.Picasso;

import java.util.HashSet;
import java.util.Set;

import static com.ominext.hdviet.adapter.MovieOfCategoryAdapter.MOVIE_ID;

public class DetailActivity extends AppCompatActivity implements OnRestClientLoadDataCompleteListener, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, View.OnLongClickListener {

    public static final String KEY_VIDEO = "KEY_VIDEO";
    public static final String MOVIE_SAVED = "MOVIE_SAVED";

    private Toolbar mToolbar;
    private ProgressDialog dialog;
    private ImageView imgBackEpisode;
    private FloatingActionButton btPlay, btShare, btSave;
    private ImageView imgDetail, imgThumbnailDetail;
    private TextView tvMovieNameVI, tvMovieNameEN, tvDescriptionMovie, tvMore, tvEpisode, tvRate;
    private TextView tvNamPhatHanh, tvDienVien, tvNuoc;

    private CardView cardEpisode;
    private MovieDetailAPI mMovieDetailAPI;

    private RecyclerView recyclerViewRelativeMovie;
    private DrawerLayout mDrawer;
    private NavigationView navigationView;
    private TextView tvHeader;
    private MenuItem mPreviousItem;
    private String movieId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        showDialog();

        movieId = getIntent().getStringExtra(MOVIE_ID);
        RestClient restClient = new RestClient();
        restClient.getMovieDetailData(movieId, 0);
        restClient.setLoadDataComplete(this);
    }

    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLoadCategoryComplete(CategoryAPI categoryAPI) {

    }

    @Override
    public void onLoadHomepageComplete(HomepageAPI homepageAPI) {

    }

    @Override
    public void onLoadMovieDetailComplete(MovieDetailAPI movieDetailAPI) {
        mMovieDetailAPI = movieDetailAPI;

        dismissDialog();

        initViews();
        initNavigation();
        initToolBar();
        setDataViews();
        addAction();
    }

    private void initNavigation() {
        Menu menu = navigationView.getMenu();

        int sizeCategory = Integer.parseInt(mMovieDetailAPI.getR().getEpisode());
        if (sizeCategory <= 0) {

            menu.add(R.id.episode_movie, Menu.NONE,
                    Menu.NONE,
                    "Đây là phim lẻ (một tập)")
                    .setIcon(R.drawable.ic_videocam_24dp);
        } else {
            for (int i = 0; i < sizeCategory; i++) {
                menu.add(R.id.episode_movie, Menu.NONE,
                        Menu.NONE,
                        "Tập " + (i + 1))
                        .setIcon(R.drawable.ic_videocam_24dp);
            }
        }
    }

    private void showDialog() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("loading...");
        dialog.show();
    }

    private void dismissDialog() {
        dialog.dismiss();
    }

    private void addAction() {
        btPlay.setOnClickListener(this);
        btShare.setOnClickListener(this);
        btSave.setOnClickListener(this);
        btSave.setOnLongClickListener(this);
        tvMore.setOnClickListener(this);
        tvEpisode.setOnClickListener(this);
        imgBackEpisode.setOnClickListener(this);
    }

    private void setDataViews() {
        tvMovieNameVI.setText(mMovieDetailAPI.getR().getKnownAs());
        tvMovieNameEN.setText(mMovieDetailAPI.getR().getMovieName());
        tvDescriptionMovie.setText(mMovieDetailAPI.getR().getPlotVI());
        tvRate.setText(mMovieDetailAPI.getR().getImdbRating());
        tvHeader.setText(tvMovieNameVI.getText().toString());

        tvNuoc.setText("Quốc gia: " + mMovieDetailAPI.getR().getCountry());
        tvDienVien.setText("Diễn viên: \n" + mMovieDetailAPI.getR().getCast());
        tvNamPhatHanh.setText("Năm phát hành: " + mMovieDetailAPI.getR().getReleaseDate());

        mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        Picasso.with(this).load(mMovieDetailAPI.getR().getNewBackdrop945x530()).into(imgDetail);
        Picasso.with(this).load(mMovieDetailAPI.getR().getPoster155x230()).into(imgThumbnailDetail);

        recyclerViewRelativeMovie.setAdapter(new MovieRelativeAdapter(mMovieDetailAPI.getR().getRelative(), this));
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void initViews() {

        tvRate = (TextView) findViewById(R.id.rate);
        tvMore = (TextView) findViewById(R.id.tv_more);
        tvEpisode = (TextView) findViewById(R.id.tv_episode);
        imgDetail = (ImageView) findViewById(R.id.img_detail);
        btPlay = (FloatingActionButton) findViewById(R.id.fab_play);
        btSave = (FloatingActionButton) findViewById(R.id.fab_save);
        btShare = (FloatingActionButton) findViewById(R.id.fab_share);
        mDrawer = (DrawerLayout) findViewById(R.id.draw_detail);
        cardEpisode = (CardView) findViewById(R.id.card_episode);
        tvMovieNameVI = (TextView) findViewById(R.id.tv_title_movie_vi);
        tvMovieNameEN = (TextView) findViewById(R.id.tv_title_movie_en);
        tvHeader = (TextView) findViewById(R.id.tv_tenphim_detail_navi);
        imgBackEpisode = (ImageView) findViewById(R.id.img_back_episode);
        navigationView = (NavigationView) findViewById(R.id.navi_episode);
        tvDescriptionMovie = (TextView) findViewById(R.id.tv_description_movie);
        imgThumbnailDetail = (ImageView) findViewById(R.id.img_thumbnail_detail);

        tvNamPhatHanh = (TextView) findViewById(R.id.tv_nam_phat_hanh);
        tvDienVien = (TextView) findViewById(R.id.tv_dien_vien);
        tvNuoc = (TextView) findViewById(R.id.tv_nuoc);


        recyclerViewRelativeMovie = (RecyclerView) findViewById(R.id.recycler_view_relative);
        recyclerViewRelativeMovie.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerViewRelativeMovie.setHasFixedSize(true);


        tvDescriptionMovie.setMaxLines(4);
    }

    @Override
    public void onLoadMovieOfCategoryComplete(MovieOfCategoryAPI movieOfCategoryAPI) {

    }

    @Override
    public void onLoadMoviePlayComplete(MoviePlayAPI moviePlayAPI) {

    }

    @Override
    public void onLoadDataSearchComplete(SearchAPI searchAPI) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_play:

                Toast.makeText(this, "Playing video...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(DetailActivity.this, YoutubeActivity.class);
                intent.putExtra(KEY_VIDEO, mMovieDetailAPI.getR().getTrailer());
                startActivity(intent);
                break;

            case R.id.fab_share:

                shareIt();
                break;

            case R.id.fab_save:
                saveMovie();
                break;

            case R.id.tv_more:
                if (tvDescriptionMovie.getMaxLines() == 4) {
                    tvDescriptionMovie.setMaxLines(100);
                    tvMore.setText("ĐÓNG");
                } else {
                    tvDescriptionMovie.setMaxLines(4);
                    tvMore.setText("XEM THÊM");
                }
                break;

            case R.id.tv_episode:
                mDrawer.openDrawer(GravityCompat.START);
                break;

            case R.id.img_back_episode:
                Animation animation2 = AnimationUtils.loadAnimation(this, R.anim.translate_episode_down);
                cardEpisode.startAnimation(animation2);
                cardEpisode.setVisibility(View.INVISIBLE);
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Toast.makeText(this, "" + item.getTitle(), Toast.LENGTH_SHORT).show();

        if(item.getItemId() == R.id.phim_da_luu){
            startActivity(new Intent(DetailActivity.this,SavedMovieActivity.class));
            mDrawer.closeDrawers();
            return true;
        }

        item.setChecked(true);
        if (mPreviousItem != null && mPreviousItem.getItemId() != item.getItemId()) {
            mPreviousItem.setChecked(false);
        }
        mPreviousItem = item;
        mDrawer.closeDrawers();
        return true;
    }

    private void shareIt() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "" + mMovieDetailAPI.getR().getKnownAs());
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, mMovieDetailAPI.getR().getTrailer());
        startActivity(Intent.createChooser(sharingIntent, "Chia sẻ với"));
    }

    private void saveMovie() {

        SharedPreferences preferences = getSharedPreferences(MOVIE_SAVED, MODE_APPEND);
        SharedPreferences.Editor editor = preferences.edit();

        Set<String> stringSet = preferences.getStringSet(MOVIE_SAVED, null);
        if (stringSet == null) {
            stringSet = new HashSet<>();
        }
        stringSet.add(
                movieId + "~" +
                        mMovieDetailAPI.getR().getKnownAs() + "~" +
                        mMovieDetailAPI.getR().getRuntime() + "~" +
                        mMovieDetailAPI.getR().getImdbRating() + "~" +
                        mMovieDetailAPI.getR().getNewBackdrop945x530());

        editor
                .putStringSet(MOVIE_SAVED, stringSet)
                .commit();

        Set<String> stringSet1 = preferences.getStringSet(MOVIE_SAVED, null);
        Toast.makeText(this, "Đã lưu " + (stringSet1.size())+ " phim...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onLongClick(View v) {
        startActivity(new Intent(DetailActivity.this,SavedMovieActivity.class));
        return true;
    }
}
