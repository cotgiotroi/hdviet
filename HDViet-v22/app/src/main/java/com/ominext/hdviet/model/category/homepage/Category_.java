
package com.ominext.hdviet.model.category.homepage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category_ {

    @SerializedName("CategoryName")
    @Expose
    private String categoryName;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Category_() {
    }

    /**
     * 
     * @param categoryName
     */
    public Category_(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * 
     * @return
     *     The categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * 
     * @param categoryName
     *     The CategoryName
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
