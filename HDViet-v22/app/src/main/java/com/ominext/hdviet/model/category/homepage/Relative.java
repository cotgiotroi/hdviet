
package com.ominext.hdviet.model.category.homepage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Relative {

    @SerializedName("MovieID")
    @Expose
    private String movieID;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Poster")
    @Expose
    private String poster;
    @SerializedName("Poster100x149")
    @Expose
    private String poster100x149;
    @SerializedName("Poster124x184")
    @Expose
    private String poster124x184;
    @SerializedName("Poster155x230")
    @Expose
    private String poster155x230;
    @SerializedName("Poster214x321")
    @Expose
    private String poster214x321;
    @SerializedName("KnownAs")
    @Expose
    private String knownAs;
    @SerializedName("WrapLink")
    @Expose
    private String wrapLink;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Relative() {
    }

    /**
     * 
     * @param movieID
     * @param poster100x149
     * @param knownAs
     * @param wrapLink
     * @param poster155x230
     * @param name
     * @param poster214x321
     * @param poster
     * @param poster124x184
     */
    public Relative(String movieID, String name, String poster, String poster100x149, String poster124x184, String poster155x230, String poster214x321, String knownAs, String wrapLink) {
        this.movieID = movieID;
        this.name = name;
        this.poster = poster;
        this.poster100x149 = poster100x149;
        this.poster124x184 = poster124x184;
        this.poster155x230 = poster155x230;
        this.poster214x321 = poster214x321;
        this.knownAs = knownAs;
        this.wrapLink = wrapLink;
    }

    /**
     * 
     * @return
     *     The movieID
     */
    public String getMovieID() {
        return movieID;
    }

    /**
     * 
     * @param movieID
     *     The MovieID
     */
    public void setMovieID(String movieID) {
        this.movieID = movieID;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The Name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The poster
     */
    public String getPoster() {
        return poster;
    }

    /**
     * 
     * @param poster
     *     The Poster
     */
    public void setPoster(String poster) {
        this.poster = poster;
    }

    /**
     * 
     * @return
     *     The poster100x149
     */
    public String getPoster100x149() {
        return poster100x149;
    }

    /**
     * 
     * @param poster100x149
     *     The Poster100x149
     */
    public void setPoster100x149(String poster100x149) {
        this.poster100x149 = poster100x149;
    }

    /**
     * 
     * @return
     *     The poster124x184
     */
    public String getPoster124x184() {
        return poster124x184;
    }

    /**
     * 
     * @param poster124x184
     *     The Poster124x184
     */
    public void setPoster124x184(String poster124x184) {
        this.poster124x184 = poster124x184;
    }

    /**
     * 
     * @return
     *     The poster155x230
     */
    public String getPoster155x230() {
        return poster155x230;
    }

    /**
     * 
     * @param poster155x230
     *     The Poster155x230
     */
    public void setPoster155x230(String poster155x230) {
        this.poster155x230 = poster155x230;
    }

    /**
     * 
     * @return
     *     The poster214x321
     */
    public String getPoster214x321() {
        return poster214x321;
    }

    /**
     * 
     * @param poster214x321
     *     The Poster214x321
     */
    public void setPoster214x321(String poster214x321) {
        this.poster214x321 = poster214x321;
    }

    /**
     * 
     * @return
     *     The knownAs
     */
    public String getKnownAs() {
        return knownAs;
    }

    /**
     * 
     * @param knownAs
     *     The KnownAs
     */
    public void setKnownAs(String knownAs) {
        this.knownAs = knownAs;
    }

    /**
     * 
     * @return
     *     The wrapLink
     */
    public String getWrapLink() {
        return wrapLink;
    }

    /**
     * 
     * @param wrapLink
     *     The WrapLink
     */
    public void setWrapLink(String wrapLink) {
        this.wrapLink = wrapLink;
    }

}
