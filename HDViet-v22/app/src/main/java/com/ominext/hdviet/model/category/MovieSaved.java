package com.ominext.hdviet.model.category;

/**
 * Created by 20133 on 10/21/2016.
 */

public class MovieSaved {
    private String id;
    private String name;
    private String duration;
    private String rate;
    private String linkBG;

    public MovieSaved(String id, String name, String duration, String rate, String linkBG) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.rate = rate;
        this.linkBG = linkBG;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getLinkBG() {
        return linkBG;
    }

    public void setLinkBG(String linkBG) {
        this.linkBG = linkBG;
    }
}
